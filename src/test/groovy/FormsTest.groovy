import com.reviselabs.validation.Form
import com.reviselabs.validation.example.Post
import com.reviselabs.validation.example.PostForm

class FormsTest extends GroovyTestCase {

  void testGetAs() {
      Form<PostForm> filledForm = Form.form(PostForm).bind(new PostForm());
      def post = filledForm.getAs(Post);
      assertNotNull(post);
      assertFalse(post.comments.empty);
  }

  void testGetSubObject() {
    Form<PostForm> filledForm = Form.form(PostForm).bind(new PostForm());
    def postForm = filledForm.get();
    assertNotNull(postForm)
    assertEquals(postForm.author.name, "Venkat");
  }

  void testBindFromObject() {
    Form<PostForm> filledForm = Form.form(PostForm).bind(new Post(title: "A new post", body: "Fresh page"));
    def postForm = filledForm.get()
    assertNotNull(postForm)
    assertEquals(postForm.title, "A new post");
    assertEquals(postForm.body, "Fresh page");
  }

  void testCopyTo() {
    def post = new Post()
    def frmPost = Form.form(PostForm) // Not filling it here since PostForm constructor sets fields.
    frmPost.copyTo(post)
    assertNotNull(post.title)
    assertEquals(post.title, frmPost.get().title)
  }
}
