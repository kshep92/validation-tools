package com.reviselabs.validation.example;

/**
 * Created by ksheppard on 09/11/2016.
 */
public class Record {
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
