package com.reviselabs.validation
import groovy.json.JsonOutput
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
/**
 * Class used to validate Java Beans and Groovy POGOs
 *
 * Usage example:
 * Form<User> userForm = Form.create(User.class).bind(Object|Map<String, Object>|Iterable<Map<String, Object>>)
 * if(userForm.valid()) { User user = userForm.get() }
 * else { renderJson(userForm.errorsAsJson()) }
 */
class Form<T> {

  private List<Error> errors
  private Validator validator
  private Logger logger
  private T object

  private Form() {
    errors = new ArrayList<>()
    logger = LoggerFactory.getLogger(getClass())
    validator = Validation.buildDefaultValidatorFactory().getValidator()
  }

  static <T> Form<T> form(Class<T> type) {
    Form<T> blankForm = new Form()
    blankForm.object = type.newInstance()
    return blankForm
  }

  /**
   * Ad-hoc convenience method to add errors to the list of errors. Useful for custom validations.
   * Use within an overridden validate() method.
   *
   * E.g. @Override public void validate() {
   *     if(birthDay.before(1990)) addError(\"birthday\", \"You are too young to use this site\");
   * }
   * @param key The field name.
   * @param value The error message.
   */
  void addError(String key, String value) {
    errors.add(new Error(field: key, message: value))
  }

  /**
   * Use the properties of an object to fill this form
   * @param source The source object
   */
  Form bind(Object source) {
    if(source instanceof T) {
      object = source
    } else map(source, object)
    return this
  }

  /**
   * Copy this form's properties to another bean
   * @param target The bean to which properties will be copied.
   */
  def copyTo(Object target) {
    map(object, target)
  }

  /**
   * Get the underlying form object
   * @return The underlying form object.
   */
  public T get() {
    return object;
  }

  /**
   * Creates a new instance of a class initialized with the form values.
   *
   * @param type The class to instantiate and initialize.
   * @param <T> The type of object it would return.
   * @return An object of type <T>
   **/
  def <T> T getAs(Class<T> type) {
    T target = type.newInstance()
    map(object, target)
    return target
  }

  /**
   * Helper method to return the errors as a JSON string.
   * @return JSON representation of validation errors.
   */
  String getErrorsAsJson() {
    return JsonOutput.toJson(errors)
  }


  /**
   * For instances where you need to get the raw errors in a Map
   * @return a Map of errors on the form
   */
  def getErrors() { errors }

  /**
   * Copies the field values of one bean to another
   * @param source The bean with the desired values
   * @param target The bean to which we write the values to
   */
  private void map(Object source, Object target) {
    def sourceProps = source.properties
    def targetProps = target.properties
    sourceProps.remove('class')
    sourceProps.remove('metaClass')
    sourceProps.each { Object k, Object v ->
        try {
            if(targetProps.containsKey(k)) target.putAt(k.toString(), v)
        } catch(Exception e) {
            logger.error(e.message)
        }
    }
  }

  Boolean valid() {
    validate()
    return errors.empty
  }

  /**
   * Internal method for validating the object and setting errors, if any.
   */
  private void validate() {
    errors = new ArrayList<>()
    Set<ConstraintViolation<T>> violations = validator.validate(object)
    if(!violations.empty) violations.each {
        errors.add(new Error(field: it.propertyPath.toString(), message: it.message))
    }
  }
}
