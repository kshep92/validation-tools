package com.reviselabs.validation.constraints;

import com.reviselabs.validation.validators.RequiredValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by ksheppard on 03/08/2016.
 */
@Target(FIELD)
@Retention(RUNTIME)
@Constraint(validatedBy = RequiredValidator.class)
public @interface Required {
    String message() default "This field is required.";

    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
